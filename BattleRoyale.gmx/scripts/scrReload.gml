///scrReload()
if (global.weaponEquiped == inventory.bow) {
    if (global.ammoEquiped == inventory.arrow) {
        var ammoToEquip = 10 - global.ammoReloaded;
        
        if (ammoToEquip >= global.ammoCount) {   
            global.ammoReloaded += global.ammoCount;         
            global.ammoCount = 0;
            global.ammoEquiped = inventory.none;
        }
        else {
            global.ammoReloaded += ammoToEquip;
            global.ammoCount -= ammoToEquip;
        }
    } 
}

else if (global.weaponEquiped == inventory.gun) {
    if (global.ammoEquiped == inventory.gunBullet) {
        var ammoToEquip = 12 - global.ammoReloaded;
        
        if (ammoToEquip > 0 and global.ammoCount > 0) {
            //Ony send the sound if it's actually reloading, y'know
            ClientSendSound(x, y, soundEffect.gunReload);
            //Only wait to reload if it's reloading
            reloading = true;
            alarm[4] = room_speed * 4;
        }
        
        if (ammoToEquip >= global.ammoCount) {   
            global.ammoReloaded += global.ammoCount;         
            global.ammoCount = 0;
            global.ammoEquiped = inventory.none;
        }
        else {
            global.ammoReloaded += ammoToEquip;
            global.ammoCount -= ammoToEquip;
        }
    } 
}

else if (global.weaponEquiped == inventory.shotgun) {
    if (global.ammoEquiped == inventory.shotgunBullet) {
        var ammoToEquip = 6 - global.ammoReloaded;
        
        if (ammoToEquip > 0 and global.ammoCount > 0) {
            //Ony send the sound if it's actually reloading, y'know
            ClientSendSound(x, y, soundEffect.shotgunReloadAll);
            //Only wait to reload if it's reloading
            reloading = true;
            alarm[4] = room_speed * 2;
        }
        
        if (ammoToEquip >= global.ammoCount) {   
            global.ammoReloaded += global.ammoCount;         
            global.ammoCount = 0;
            global.ammoEquiped = inventory.none;
        }
        else {
            global.ammoReloaded += ammoToEquip;
            global.ammoCount -= ammoToEquip;
        }
    } 
}
