/// scrClickInventoryX(mouseX)
// Returns the position X of the inventory that you clicked
// or -1 if it was outside the inventory
var mouseX = argument[0];

if (mouseX < view_xview[0] + 460 or mouseX > view_xview[0] + 460 + 128) {
    return -1;
}


var xx = (mouseX - (view_xview[0] + 460)) div inventorySize;

return xx;
