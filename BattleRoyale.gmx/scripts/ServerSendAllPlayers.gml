///ServerSendAllPlayers(playerID, playerSocket)
var playerID = argument[0];
var playerSocket = argument[1];
/// This here is gonna send the coordinates/clothes/whatever to 
/// the player who just entered of everybody who is already on the server


//The number of sockets = the number of players connected
var playerCount = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_CREATE_PLAYER);
buffer_write(global.bufferServerWrite, buffer_u16, playerCount);

//Write a really big packet with everybody information
for(var i = 0; i < playerCount; i++) {
    var thisPlayerID = ds_list_find_value(global.socketList, i);
    var thisPlayer = ds_map_find_value(global.Clients, thisPlayerID);
    //I really don't need to send to the player his own information, right?
    if (thisPlayerID != playerID) {
        buffer_write(global.bufferServerWrite, buffer_u32, thisPlayerID);  //Remember, kids, your ID is your socket, not your player.id!
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.x);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.y);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.sprite_index);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.image_index);
        buffer_write(global.bufferServerWrite, buffer_s16, thisPlayer.myHair);
        buffer_write(global.bufferServerWrite, buffer_s16, thisPlayer.myShirt);
        buffer_write(global.bufferServerWrite, buffer_s16, thisPlayer.myLegs);
        buffer_write(global.bufferServerWrite, buffer_s16, thisPlayer.myHat);
        buffer_write(global.bufferServerWrite, buffer_s16, thisPlayer.myShoes);
        buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.myHairColor);
        buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.myShirtColor);
        buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.myLegsColor);
        buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.myHatColor);
        buffer_write(global.bufferServerWrite, buffer_s32, thisPlayer.myShoesColor);
        buffer_write(global.bufferServerWrite, buffer_u16, thisPlayer.playerLife);
    }
}
/*
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
} */

network_send_packet(playerSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));

    
