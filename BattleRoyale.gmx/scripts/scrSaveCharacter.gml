///scrSaveCharacter(saveSlot)
saveSlot = argument[0];

ini_open("Characters.sav");

ini_write_real(saveSlot, "Sprite", global.playerSprite);
ini_write_real(saveSlot, "Hat", global.playerHat);
ini_write_real(saveSlot, "Hair", global.playerHair);
ini_write_real(saveSlot, "Shirt", global.playerShirt);
ini_write_real(saveSlot, "Legs", global.playerLegs);
ini_write_real(saveSlot, "Shoes", global.playerShoes);

ini_write_real(saveSlot, "Hat Color", global.playerHatColor);
ini_write_real(saveSlot, "Hair Color", global.playerHairColor);
ini_write_real(saveSlot, "Shirt Color", global.playerShirtColor);
ini_write_real(saveSlot, "Legs Color", global.playerLegsColor);
ini_write_real(saveSlot, "Shoes Color", global.playerShoesColor);

ini_close();

