///scrServerBowAttack(direction, player, angle)

var playerDirection = argument[0];
var playerInstance = argument[1];
var arrowAngle = argument[2];

switch(playerDirection) {
    case "UP":
        with (playerInstance) {
            var arrow = instance_create(x + 12, y - 64, objArrow);
            arrow.sprite_index = sprArrowUp;
            arrow.angle = arrowAngle;
            ds_list_add(global.arrowsListServer, arrow);
        }
        break;
    case "DOWN":
        with (playerInstance) {
            var arrow = instance_create(x + 12, y + 64, objArrow);
            arrow.sprite_index = sprArrowDown;
            arrow.angle = arrowAngle;
            ds_list_add(global.arrowsListServer, arrow);
        }

        break;
    case "LEFT":
        with (playerInstance) {
            var arrow = instance_create(x - 38, y + 16, objArrow);
            arrow.sprite_index = sprArrowLeft;
            arrow.angle = arrowAngle;
            ds_list_add(global.arrowsListServer, arrow);
        }

        break;
    case "RIGHT":
        with (playerInstance) {
            var arrow = instance_create(x + 48, y + 16, objArrow);
            arrow.sprite_index = sprArrowRight;
            arrow.angle = arrowAngle;
            ds_list_add(global.arrowsListServer, arrow);
        }

        break;
}

ServerSendBowAttack(arrow, arrow.x, arrow.y, arrow.sprite_index);
