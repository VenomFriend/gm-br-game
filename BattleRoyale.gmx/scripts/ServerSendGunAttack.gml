var bulletInstance = argument[0];
var bulletX = argument[1];
var bulletY = argument[2];
var bulletSprite = argument[3];
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_GUN);
buffer_write(global.bufferServerWrite, buffer_u32, bulletInstance);
buffer_write(global.bufferServerWrite, buffer_u16, bulletX);
buffer_write(global.bufferServerWrite, buffer_u16, bulletY);
buffer_write(global.bufferServerWrite, buffer_u16, bulletSprite);
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}

