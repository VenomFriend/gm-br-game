/// scrAddToInventory(item, count)
// Returns true if the item was added, false if not
var itemToAdd = argument[0];
var numberToAdd = argument[1];

// If it's an item that ocupy a single box
if ( (itemToAdd == inventory.arrow) or (itemToAdd == inventory.gunBullet) or (itemToAdd == inventory.shotgunBullet)) {
    for (var yy = 0; yy < inventoryHeight; yy++) {
        for (var xx = 0; xx < inventoryWidth; xx++) {
            //If it found the item on the inventory, add to the count!
            if (inventoryItems[# xx, yy] == itemToAdd) {
                inventoryValues[# xx, yy] += numberToAdd;
                return true;
            }
        }
    }
}

//Add the item to an empty space, if it exists
for (var yy = 0; yy < inventoryHeight; yy++) {
    for (var xx = 0; xx < inventoryWidth; xx++) {
        //Add the item
        if (inventoryItems[# xx, yy] == inventory.none) {
            inventoryItems[# xx, yy] = itemToAdd;
            inventoryValues[# xx, yy] = numberToAdd;
            return true;
        }
    }
}

//If it didn't return true, it's because the inventory is already full
return false;
