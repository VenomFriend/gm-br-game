# Joguin #

Little game being made by Venom Friend, on GM Studio.

### Devs ###

* Venom Friend

### "Designers" ###

* Mau-Mau

### Testers ###

* Nemesys "Eustácio"
* Mau-Mau "Mauriçador"

## Features ##

### DONE: ###

* Criação de personagem:
    * Chapéu, cabelo e tom de pele pra escolher;
    * Cores(milhões delas!);
    * Salvar/Ler personagens(até 6);
* Menu bonitinho (a não ser que Mau-Mau vá editar mais);
* Cliente/Servidor funcionando;
* Campo de visão:
    * Cone de visão;
    * Não vê através de paredes/objetos;
    * Fade in/Fade out;
* Sons direcionais;
* Hitbox de ataque;
* Drop de item ao morrer;
* Criação/destruição de projéteis ao sairem/baterem em algo;
* Inventário;
* Armas:
    * Faca;
    * Sabre;
    * Shotgun;
    * Pistola;
    * Arco;
    * Usar munição;
    * Carregar/recarregar arma;
    * Som de tiro/carregamento;
* Itens:
    * Colete;
    * Medkit;
    * Stamina;
    * Munição/Flechas;
* Usar SourceTree + BitBucket pro repositorio do meu jogo;