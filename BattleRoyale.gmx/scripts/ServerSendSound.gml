///ServerSendSound(x, y, sound)

var xx = argument[0];
var yy = argument[1];
var soundType = argument[2];
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_SOUND);
buffer_write(global.bufferServerWrite, buffer_u16, xx);
buffer_write(global.bufferServerWrite, buffer_u16, yy);
buffer_write(global.bufferServerWrite, buffer_u16, soundType);

for(var i = 0; i<socketSize; i++) {
    var thisSocket = ds_list_find_value(global.socketList, i);
    network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}

