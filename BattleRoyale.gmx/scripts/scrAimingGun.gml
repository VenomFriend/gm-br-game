switch(global.directionPlayer) {
    case "UP":
         if (keyboard_check(vk_left) and global.angleBow < 135) {
            global.angleBow += 5;
        }
        else if (keyboard_check(vk_right) and global.angleBow > 45) {
            global.angleBow -= 5;
        }   
        break;
    case "DOWN":
        if (keyboard_check(vk_left) and global.angleBow > 225) {
            global.angleBow -= 5;
        }
        else if (keyboard_check(vk_right) and global.angleBow < 310) {
            global.angleBow += 5;
        }   
        break;
    case "LEFT":
        if (keyboard_check(vk_up) and global.angleBow > 135) {
            global.angleBow -= 5;
        }
        else if (keyboard_check(vk_down) and global.angleBow < 225) {
            global.angleBow += 5;
        }   
        break;
    case "RIGHT":
        if (keyboard_check(vk_up) and global.angleBow < 405) {
            global.angleBow += 5;
        }
        else if (keyboard_check(vk_down) and global.angleBow > 315) {
            global.angleBow -= 5;
        }   
        break;
}
if keyboard_check_released(vk_control) {
    //Only shoot if there are bullets, and they are the correct ones!
    var bulletType;
    if (global.weaponEquiped == inventory.gun) {
        bulletType = inventory.gunBullet;
    } else if (global.weaponEquiped == inventory.shotgun) {
        bulletType = inventory.shotgunBullet;
    }
    
    if (global.ammoReloaded > 0) {
        global.ammoReloaded--;
        ClientSendAttack();     
        if (global.weaponEquiped == inventory.gun) {
            justshot = true;
            alarm[5] = room_speed * 0.5;
            ClientSendSound(x, y, soundEffect.gunShot);
        }
        else if(global.weaponEquiped == inventory.shotgun) {
            justshot = true;
            alarm[5] = room_speed * 1;
            ClientSendSound(x, y, soundEffect.shotgunShot);
        }
    } 
    else {
        //Yeah, it's empty
        ClientSendSound(x, y, soundEffect.shotgunEmpty);
    }
    alarm[1] = room_speed * 0.5;       
}
