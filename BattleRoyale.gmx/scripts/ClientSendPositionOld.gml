var inst = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_MOVEMENT);
buffer_write(global.bufferClientWrite, buffer_u32, global.myPlayerID);
buffer_write(global.bufferClientWrite, buffer_u16, inst.x);
buffer_write(global.bufferClientWrite, buffer_u16, inst.y);
buffer_write(global.bufferClientWrite, buffer_u16, inst.sprite_index);
buffer_write(global.bufferClientWrite, buffer_u16, inst.image_index);
buffer_write(global.bufferClientWrite, buffer_s16, global.playerHair);
buffer_write(global.bufferClientWrite, buffer_s16, global.playerShirt);
buffer_write(global.bufferClientWrite, buffer_s16, global.playerLegs);
buffer_write(global.bufferClientWrite, buffer_s16, global.playerHat);
buffer_write(global.bufferClientWrite, buffer_s16, global.playerShoes);
buffer_write(global.bufferClientWrite, buffer_s32, global.playerHairColor);
buffer_write(global.bufferClientWrite, buffer_s32, global.playerShirtColor);
buffer_write(global.bufferClientWrite, buffer_s32, global.playerLegsColor);
buffer_write(global.bufferClientWrite, buffer_s32, global.playerHatColor);
buffer_write(global.bufferClientWrite, buffer_s32, global.playerShoesColor);
buffer_write(global.bufferClientWrite, buffer_u16, global.playerLife);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
