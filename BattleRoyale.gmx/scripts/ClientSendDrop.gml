///ClientSendDrop(item, quantity, playerX, playerY)
//Drops the item!

var itemDropped = argument[0];
var quantity = argument[1];
var playerX = argument[2];
var playerY = argument[3];

var itemCount = ds_list_size(itemDropped);

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_BACKPACK);
buffer_write(global.bufferClientWrite, buffer_u16, itemCount);
for(var i = 0; i < itemCount; i++) {
    buffer_write(global.bufferClientWrite, buffer_u16, itemDropped[| i]);
    buffer_write(global.bufferClientWrite, buffer_u16, quantity[| i]);
}
buffer_write(global.bufferClientWrite, buffer_u16, playerX);
buffer_write(global.bufferClientWrite, buffer_u16, playerY);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
