///ClientSendTookItem(backpackID, itemPosition)
var thisID = argument[0];
var thisPosition = argument[1];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_TOOK_FROM_BACKPACK);
buffer_write(global.bufferClientWrite, buffer_u32, thisID);
buffer_write(global.bufferClientWrite, buffer_u16, thisPosition);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
