switch(global.directionPlayer) {
    case "UP":
         if (keyboard_check(vk_left) and global.angleBow < 135) {
            global.angleBow += 5;
        }
        else if (keyboard_check(vk_right) and global.angleBow > 45) {
            global.angleBow -= 5;
        }   
        break;
    case "DOWN":
        if (keyboard_check(vk_left) and global.angleBow > 225) {
            global.angleBow -= 5;
        }
        else if (keyboard_check(vk_right) and global.angleBow < 310) {
            global.angleBow += 5;
        }   
        break;
    case "LEFT":
        if (keyboard_check(vk_up) and global.angleBow > 135) {
            global.angleBow -= 5;
        }
        else if (keyboard_check(vk_down) and global.angleBow < 225) {
            global.angleBow += 5;
        }   
        break;
    case "RIGHT":
        if (keyboard_check(vk_up) and global.angleBow < 405) {
            global.angleBow += 5;
        }
        else if (keyboard_check(vk_down) and global.angleBow > 315) {
            global.angleBow -= 5;
        }   
        break;
}
if keyboard_check_released(vk_control) {
    aimingWithBow = false;
    image_speed = 0.5;
}
