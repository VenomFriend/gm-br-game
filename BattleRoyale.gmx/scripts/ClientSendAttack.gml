switch global.weaponEquiped {
    // Melee
    case inventory.none:
    // Knife
    case inventory.knife:
    // Rapier
    case inventory.rapier:
        buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
        buffer_write(global.bufferClientWrite, buffer_u8, MSG_MELEE);
        buffer_write(global.bufferClientWrite, buffer_u32, global.myPlayerID);
        buffer_write(global.bufferClientWrite, buffer_string, global.directionPlayer);
        buffer_write(global.bufferClientWrite, buffer_u16, global.weaponEquiped);
        network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
        break;
    //Bow
    case inventory.bow:
        buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
        buffer_write(global.bufferClientWrite, buffer_u8, MSG_BOW);
        buffer_write(global.bufferClientWrite, buffer_u32, global.myPlayerID);
        buffer_write(global.bufferClientWrite, buffer_string, global.directionPlayer);
        buffer_write(global.bufferClientWrite, buffer_u16, global.angleBow);
        network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
        break;
    //Gun
    case inventory.gun:
        buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
        buffer_write(global.bufferClientWrite, buffer_u8, MSG_GUN);
        buffer_write(global.bufferClientWrite, buffer_u32, global.myPlayerID);
        buffer_write(global.bufferClientWrite, buffer_string, global.directionPlayer);
        buffer_write(global.bufferClientWrite, buffer_u16, global.angleBow);
        network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
        break;
    //Shotgun
    case inventory.shotgun:
        buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
        buffer_write(global.bufferClientWrite, buffer_u8, MSG_SHOTGUN);
        buffer_write(global.bufferClientWrite, buffer_u32, global.myPlayerID);
        buffer_write(global.bufferClientWrite, buffer_string, global.directionPlayer);
        buffer_write(global.bufferClientWrite, buffer_u16, global.angleBow);
        network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
        break;
}
