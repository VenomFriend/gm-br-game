/// scrClickInventoryX(mouseX)
// Returns the position X of the inventory that you clicked
// or -1 if it was outside the inventory
var mouseX = argument[0];
var sizeOfBackpack = ds_list_size(items);
if (mouseX < view_xview[0] + 200 or mouseX > view_xview[0] + 200 + (sizeOfBackpack * 32)) {
    return -1;
}


var xx = (mouseX - (view_xview[0] + 200)) div 32;

return xx;
