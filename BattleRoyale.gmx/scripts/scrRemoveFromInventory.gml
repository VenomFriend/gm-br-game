/// scrRemoveFromInventory(itemX, itemY, count)
// Returns true if the item was removed, false if not
var xx = argument[0];
var yy = argument[1];
var numberToRemove = argument[2];

//If it found the item on the inventory, remove it!
if (inventoryItems[# xx, yy] != inventory.none) {
    
    if (inventoryValues[# xx, yy] > numberToRemove) {
        inventoryValues[# xx, yy] -= numberToRemove;
    } else if (inventoryValues[# xx, yy] == numberToRemove) {
        inventoryValues[#xx, yy] = 0;
        inventoryItems[# xx, yy] = inventory.none;
    } else {
        return false;
    }
    return true;
}



//If it didn't return true, it's because you didn't had the quantity to remove
return false;
