///ClientSendSound(xx, yy, sound)
var xx = argument[0];
var yy = argument[1]
var sound = argument[2];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_SOUND);
buffer_write(global.bufferClientWrite, buffer_u16, xx);
buffer_write(global.bufferClientWrite, buffer_u16, yy);
buffer_write(global.bufferClientWrite, buffer_u16, sound);

network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
