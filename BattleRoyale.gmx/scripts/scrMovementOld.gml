///scrMovement(characterSprite, characterSpeed)
sprite = argument[0];
charSpeed = argument[1];
image_speed = 0.5;

if ( keyboard_check(vk_up) and keyboard_check(vk_left) )
{
    global.directionPlayer = "UP";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,9];
    }
    else {
        sprite_index = global.charSprites[sprite,0];
    }
    if (y > charSpeed and place_free(x, y -charSpeed))
    {
        y -= charSpeed;
    }
    if (x > charSpeed and place_free(x - charSpeed, y))
    {   
        x -= charSpeed;
    }
}
else if ( keyboard_check(vk_up) and keyboard_check(vk_right) )
{
    global.directionPlayer = "UP";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,9];
    }
    else {
        sprite_index = global.charSprites[sprite,0];
    }
    if (y > charSpeed and place_free(x, y -charSpeed))
    {
        y -= charSpeed;
    }
    if (x < room_width - charSpeed and place_free(x + charSpeed, y))
    {
        x += charSpeed;
    }
}
else if ( keyboard_check(vk_down) and keyboard_check(vk_left) )
{
    global.directionPlayer = "DOWN";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,10];
    }
    else {
        sprite_index = global.charSprites[sprite,1];
    }
    if (y < room_height - charSpeed and place_free(x, y +charSpeed))
    {
        y += charSpeed;
    }
    if (x > charSpeed and place_free(x - charSpeed, y))
    {   
        x -= charSpeed;
    }
}
else if ( keyboard_check(vk_down) and keyboard_check(vk_right) )
{
    global.directionPlayer = "DOWN";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,10];
    }
    else {
        sprite_index = global.charSprites[sprite,1];
    }
    if (y < room_height - charSpeed and place_free(x, y +charSpeed))
    {
        y += charSpeed;
    }
    if (x < room_width - charSpeed and place_free(x + charSpeed, y))
    {
        x += charSpeed;
    }
}
else if (keyboard_check(vk_left)) 
{
    global.directionPlayer = "LEFT";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,11];
    }
    else {
        sprite_index = global.charSprites[sprite,2];
    }
    if (x > charSpeed and place_free(x - charSpeed, y))
    {   
        x -= charSpeed;
    }
}
else if (keyboard_check(vk_right))
{
    global.directionPlayer = "RIGHT";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,12];
    }
    else {
        sprite_index = global.charSprites[sprite,3];
    }
    if (x < room_width - charSpeed and place_free(x + charSpeed, y))
    {
        x += charSpeed;
    }
}
else if (keyboard_check(vk_up)) 
{
    global.directionPlayer = "UP";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,9];
    }
    else {
        sprite_index = global.charSprites[sprite,0];
    }
    if (y > charSpeed and place_free(x, y -charSpeed))
    {
        y -= charSpeed;
    }
}
else if (keyboard_check(vk_down))
{
    global.directionPlayer = "DOWN";
    if global.weaponEquiped == 1 {
        sprite_index = global.charSprites[sprite,10];
    }
    else {
        sprite_index = global.charSprites[sprite,1];
    }
    if (y < room_height - charSpeed and place_free(x, y + charSpeed))
    {
        y += charSpeed;
    }
}
else 
{
    image_index = 0;
}
