///scrSaveCharacter(saveSlot)
var saveSlot = argument[0];

//ini_open("Characters.sav");

myBody = ini_read_real(saveSlot, "Sprite", sprPlayerCustomBody);
myHat = ini_read_real(saveSlot, "Hat", -1);
myHair = ini_read_real(saveSlot, "Hair", -1);
myShirt = ini_read_real(saveSlot, "Shirt", -1);
myLegs = ini_read_real(saveSlot, "Legs", -1);
myShoes = ini_read_real(saveSlot, "Shoes", -1);

myHatColor = ini_read_real(saveSlot, "Hat Color", c_white);
myHairColor = ini_read_real(saveSlot, "Hair Color", c_white);
myShirtColor = ini_read_real(saveSlot, "Shirt Color", c_white);
myLegsColor = ini_read_real(saveSlot, "Legs Color", c_white);
myShoesColor = ini_read_real(saveSlot, "Shoes Color", c_white);

//ini_close();

