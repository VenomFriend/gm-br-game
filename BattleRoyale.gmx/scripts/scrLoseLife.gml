///scrLoseLife(life)
var lifeToLose = argument[0];

if (global.playerKevlar == 0) {
    global.playerLife -= lifeToLose;
} else {
    global.playerKevlar -= lifeToLose;
    if (global.playerKevlar < 0) {
        global.playerLife += global.playerKevlar;
        global.playerKevlar = 0;
    }
}
