if (global.isHosting){
    instance_create(0, 0, objServer);
}

if (global.isConnecting) {
    instance_create(0, 0, objClient);
}

enum equipedWeapon {
    fist = 0,
    knife = 1,
    bow = 2,
    gun = 3,
    shotgun = 4
}

enum soundEffect {
    bowShot = 0,
    gunShot = 1,
    gunReload = 2,
    gunEmpty = 3,
    shotgunShot = 4,
    shotgunReload = 5,
    shotgunReloadAll = 6,
    shotgunEmpty = 7
}

//All the items that can be on the inventory
enum inventory {
    none = 0,
    knife = 1,
    bow = 2,
    gun = 3,
    shotgun = 4,
    gunBullet = 5,
    shotgunBullet = 6,
    arrow = 7,
    pill = 8,
    medkit = 9,
    kevlar = 10,
    rapier = 11
};

//Graphics:


enum playerMovement {
    upStart = 60,
    upEnd = 68,
    leftStart = 69,
    leftEnd = 77,
    downStart =   78,
    downEnd = 86,
    rightStart = 87,
    rightEnd = 95
}

enum playerAttack {
    upStart = 96,
    upEnd = 101,
    leftStart = 102,
    leftEnd = 107,
    downStart =   108,
    downEnd = 113,
    rightStart = 115,
    rightEnd = 119
}

enum playerBow {
    upStart = 120,
    upMiddle = 128,
    upEnd = 132,
    leftStart = 133,
    leftMiddle = 141,
    leftEnd = 145,
    downStart =   146,
    downMiddle = 154,
    downEnd = 158,
    rightStart = 159,
    rightMiddle = 167,
    rightEnd = 171
}

enum bowAnimation {
    upStart = 0,
    upEnd = 12,
    leftStart = 13,
    leftEnd = 25, 
    downStart = 26,
    downEnd = 38,
    rightStart = 39,
    rightEnd = 51
}

enum playerDeath {
    start = 172,
    finish = 177
}

enum playerGun {
    up = 178,
    left = 179,
    down = 180,
    right = 181
}

enum playerShotgun {
    up = 182,
    left = 183,
    down = 184,
    right = 185
}

enum playerKnife {
    upStart = 0,
    upEnd = 5,
    leftStart = 6,
    leftEnd = 11,
    downStart =   12,
    downEnd = 17,
    rightStart = 18,
    rightEnd = 23
}

enum playerRapier {
    upStart = 0,
    upEnd = 5,
    leftStart = 6,
    leftEnd = 11,
    downStart =   12,
    downEnd = 17,
    rightStart = 18,
    rightEnd = 23
}
