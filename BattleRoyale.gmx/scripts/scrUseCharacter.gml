///scrUseCharacter()

global.playerBody = myBody;
global.playerHat = myHat;
global.playerHair = myHair;
global.playerShirt = myShirt;
global.playerLegs = myLegs;
global.playerShoes = myShoes;

global.playerHatColor = myHatColor;
global.playerHairColor = myHairColor;
global.playerShirtColor = myShirtColor;
global.playerLegsColor = myLegsColor;
global.playerShoesColor = myShoesColor;
