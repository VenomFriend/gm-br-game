/*
if (global.isHosting){
    instance_create(0, 0, objServer);
}

if (global.isConnecting) {
    instance_create(0, 0, objClient);
}

enum equipedWeapon {
    fist = 0,
    knife = 1,
    bow = 2,
    gun = 3,
    shotgun = 4
}


//Graphics:


enum playerMovement {
    upStart = 60,
    upEnd = 68,
    leftStart = 69,
    leftEnd = 77,
    downStart =   78,
    downEnd = 86,
    rightStart = 87,
    rightEnd = 95
}

enum playerAttack {
    upStart = 96,
    upEnd = 101,
    leftStart = 102,
    leftEnd = 107,
    downStart =   108,
    downEnd = 113,
    rightStart = 115,
    rightEnd = 119
}

enum playerBow {
    upStart = 120,
    upMiddle = 128,
    upEnd = 132,
    leftStart = 133,
    leftMiddle = 141,
    leftEnd = 145,
    downStart =   146,
    downMiddle = 154,
    downEnd = 158,
    rightStart = 159,
    rightMiddle = 167,
    rightEnd = 171
}

enum bowAnimation {
    upStart = 0,
    upEnd = 12,
    leftStart = 13,
    leftEnd = 25, 
    downStart = 26,
    downEnd = 38,
    rightStart = 39,
    rightEnd = 51
}

enum playerDeath {
    start = 172,
    finish = 177
}

enum playerGun {
    up = 178,
    left = 179,
    down = 180,
    right = 181
}

enum playerShotgun {
    up = 182,
    left = 183,
    down = 184,
    right = 185
}

enum playerKnife {
    upStart = 0,
    upEnd = 5,
    leftStart = 6,
    leftEnd = 11,
    downStart =   12,
    downEnd = 17,
    rightStart = 18,
    rightEnd = 23
}















// Player Movement

global.charSprites[0,0] = sprPlayerUp;
global.charSprites[0,1] = sprPlayerDown;
global.charSprites[0,2] = sprPlayerLeft;
global.charSprites[0,3] = sprPlayerRight;

global.charSprites[1,0] = sprPlayer2Up;
global.charSprites[1,1] = sprPlayer2Down;
global.charSprites[1,2] = sprPlayer2Left;
global.charSprites[1,3] = sprPlayer2Right;

global.charSprites[2,0] = sprPlayer3Up;
global.charSprites[2,1] = sprPlayer3Down;
global.charSprites[2,2] = sprPlayer3Left;
global.charSprites[2,3] = sprPlayer3Right;


// Player Punch Attack

global.charSprites[0,4] = sprPlayerPunchUp;
global.charSprites[0,5] = sprPlayerPunchDown;
global.charSprites[0,6] = sprPlayerPunchLeft;
global.charSprites[0,7] = sprPlayerPunchRight;

global.charSprites[1,4] = sprPlayer2PunchUp;
global.charSprites[1,5] = sprPlayer2PunchDown;
global.charSprites[1,6] = sprPlayer2PunchLeft;
global.charSprites[1,7] = sprPlayer2PunchRight;

global.charSprites[2,4] = sprPlayer3PunchUp;
global.charSprites[2,5] = sprPlayer3PunchDown;
global.charSprites[2,6] = sprPlayer3PunchLeft;
global.charSprites[2,7] = sprPlayer3PunchRight;

// Player Death

global.charSprites[0,8] = sprPlayerDeath;

global.charSprites[1,8] = sprPlayer2Death;

global.charSprites[2,8] = sprPlayer3Death;

// Player Knife Movement

global.charSprites[0,9] = sprPlayerKnifeUp;
global.charSprites[0,10] = sprPlayerKnifeDown;
global.charSprites[0,11] = sprPlayerKnifeLeft;
global.charSprites[0,12] = sprPlayerKnifeRight;

global.charSprites[1,9] = sprPlayer2KnifeUp;
global.charSprites[1,10] = sprPlayer2KnifeDown;
global.charSprites[1,11] = sprPlayer2KnifeLeft;
global.charSprites[1,12] = sprPlayer2KnifeRight;

global.charSprites[2,9] = sprPlayer3KnifeUp;
global.charSprites[2,10] = sprPlayer3KnifeDown;
global.charSprites[2,11] = sprPlayer3KnifeLeft;
global.charSprites[2,12] = sprPlayer3KnifeRight;

// Player Knife Attack

global.charSprites[0,13] = sprPlayerKnifeAttackUp;
global.charSprites[0,14] = sprPlayerKnifeAttackDown;
global.charSprites[0,15] = sprPlayerKnifeAttackLeft;
global.charSprites[0,16] = sprPlayerKnifeAttackRight;

global.charSprites[1,13] = sprPlayer2KnifeAttackUp;
global.charSprites[1,14] = sprPlayer2KnifeAttackDown;
global.charSprites[1,15] = sprPlayer2KnifeAttackLeft;
global.charSprites[1,16] = sprPlayer2KnifeAttackRight;

global.charSprites[2,13] = sprPlayer3KnifeAttackUp;
global.charSprites[2,14] = sprPlayer3KnifeAttackDown;
global.charSprites[2,15] = sprPlayer3KnifeAttackLeft;
global.charSprites[2,16] = sprPlayer3KnifeAttackRight;

//Player Bow Attack

global.charSprites[0,17] = sprPlayerBowAttackUp;
global.charSprites[0,18] = sprPlayerBowAttackDown;
global.charSprites[0,19] = sprPlayerBowAttackLeft;
global.charSprites[0,20] = sprPlayerBowAttackRight;

global.charSprites[1,17] = sprPlayer2BowAttackUp;
global.charSprites[1,18] = sprPlayer2BowAttackDown;
global.charSprites[1,19] = sprPlayer2BowAttackLeft;
global.charSprites[1,20] = sprPlayer2BowAttackRight;

global.charSprites[2,17] = sprPlayer3BowAttackUp;
global.charSprites[2,18] = sprPlayer3BowAttackDown;
global.charSprites[2,19] = sprPlayer3BowAttackLeft;
global.charSprites[2,20] = sprPlayer3BowAttackRight;


//Player Gun Attack

global.charSprites[0,21] = sprPlayerGunAttackUp;
global.charSprites[0,22] = sprPlayerGunAttackDown;
global.charSprites[0,23] = sprPlayerGunAttackLeft;
global.charSprites[0,24] = sprPlayerGunAttackRight;

global.charSprites[1,21] = sprPlayer2GunAttackUp;
global.charSprites[1,22] = sprPlayer2GunAttackDown;
global.charSprites[1,23] = sprPlayer2GunAttackLeft;
global.charSprites[1,24] = sprPlayer2GunAttackRight;

global.charSprites[2,21] = sprPlayer3GunAttackUp;
global.charSprites[2,22] = sprPlayer3GunAttackDown;
global.charSprites[2,23] = sprPlayer3GunAttackLeft;
global.charSprites[2,24] = sprPlayer3GunAttackRight;

//Player Shotgun Attack

global.charSprites[0,25] = sprPlayerShotgunAttackUp;
global.charSprites[0,26] = sprPlayerShotgunAttackDown;
global.charSprites[0,27] = sprPlayerShotgunAttackLeft;
global.charSprites[0,28] = sprPlayerShotgunAttackRight;

global.charSprites[1,25] = sprPlayer2ShotgunAttackUp;
global.charSprites[1,26] = sprPlayer2ShotgunAttackDown;
global.charSprites[1,27] = sprPlayer2ShotgunAttackLeft;
global.charSprites[1,28] = sprPlayer2ShotgunAttackRight;

global.charSprites[2,25] = sprPlayer3ShotgunAttackUp;
global.charSprites[2,26] = sprPlayer3ShotgunAttackDown;
global.charSprites[2,27] = sprPlayer3ShotgunAttackLeft;
global.charSprites[2,28] = sprPlayer3ShotgunAttackRight;


*/
