//Create a new inventory, and make it empty
inventoryItems = ds_grid_create(inventoryWidth, inventoryHeight);
ds_grid_clear(inventoryItems, inventory.none);

//Put every value on the inventory to 0
inventoryValues = ds_grid_create(inventoryWidth, inventoryHeight);
ds_grid_clear(inventoryValues, 0);

//Start with a random weapon, to test!

/*
var weaponRandom = choose(inventory.knife, inventory.bow, inventory.gun, inventory.shotgun);

scrAddToInventory(weaponRandom, 1);
if (weaponRandom == inventory.bow) {
    scrAddToInventory(inventory.arrow, 10);
} else if (weaponRandom == inventory.gun) {
    scrAddToInventory(inventory.gunBullet, 10);
} else if (weaponRandom == inventory.shotgun) {
    scrAddToInventory(inventory.shotgunBullet, 10);
}

var quantityOfStuff = irandom(4);

for (var i = 0; i < quantityOfStuff; i++) {
    var itemRandom = choose(inventory.medkit, inventory.kevlar, inventory.pill);
    scrAddToInventory(itemRandom, 1);
}
*/

scrAddToInventory(inventory.bow, 1);
scrAddToInventory(inventory.arrow, 99);
scrAddToInventory(inventory.knife, 1);
scrAddToInventory(inventory.gun, 1);
scrAddToInventory(inventory.gunBullet, 99);
scrAddToInventory(inventory.shotgun, 1);
scrAddToInventory(inventory.shotgunBullet, 99);
scrAddToInventory(inventory.rapier, 1);
