var bufferServerReceived = argument[0];
var socket = argument[1];
var msgid = buffer_read(bufferServerReceived, buffer_u8);

switch (msgid) {
    
    //The player who connected sent me all his information, so I must update it here!
    case MSG_CREATE_ID:
        var thisID = buffer_read(bufferServerReceived, buffer_u32);
        var thisX = buffer_read(bufferServerReceived, buffer_u16);
        var thisY = buffer_read(bufferServerReceived, buffer_u16);
        var thisSprite = buffer_read(bufferServerReceived, buffer_u16);
        var thisImage = buffer_read(bufferServerReceived, buffer_u16);
        var thisHair =  buffer_read(bufferServerReceived, buffer_s16);
        var thisShirt = buffer_read(bufferServerReceived, buffer_s16);
        var thisLegs = buffer_read(bufferServerReceived, buffer_s16);
        var thisHat = buffer_read(bufferServerReceived, buffer_s16);
        var thisShoes = buffer_read(bufferServerReceived, buffer_s16);
        var thisHairColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisShirtColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisLegsColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisHatColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisShoesColor = buffer_read(bufferServerReceived, buffer_s32);
        var thisLife = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.Clients, thisID))){
            var inst = ds_map_find_value(global.Clients, thisID);
            inst.x = thisX;
            inst.y = thisY;
            inst.sprite_index = thisSprite;
            inst.image_index = thisImage; 
            inst.myHair = thisHair;
            inst.myShirt = thisShirt;
            inst.myLegs = thisLegs;
            inst.myHat = thisHat;
            inst.myShoes = thisShoes;
            inst.myHairColor = thisHairColor;
            inst.myShirtColor = thisShirtColor;
            inst.myLegsColor = thisLegsColor;
            inst.myHatColor = thisHatColor;
            inst.myShoesColor = thisShoesColor;
            inst.playerLife = thisLife;
            ServerSendNewPlayer(inst, socket);
        }
        
        break;

    // Someone moved, update every other player!
    case MSG_MOVEMENT:
        var thisID = buffer_read(bufferServerReceived, buffer_u32);
        var thisX = buffer_read(bufferServerReceived, buffer_u16);
        var thisY = buffer_read(bufferServerReceived, buffer_u16);
        var thisImage = buffer_read(bufferServerReceived, buffer_u16);
        var thisLife = buffer_read(bufferServerReceived, buffer_u16);
        
        if (!is_undefined(ds_map_find_value(global.Clients, thisID))){
            var inst = ds_map_find_value(global.Clients, thisID);
            inst.x = thisX;
            inst.y = thisY;
            inst.image_index = thisImage; 
            inst.playerLife = thisLife;
        }
           
    break;
    // A player attacked with melee, we need to see if someone was hit, and send it to everyone
    case MSG_MELEE:
        var thisID = buffer_read(bufferServerReceived, buffer_u32);
        var thisDirection = buffer_read(bufferServerReceived, buffer_string);
        var thisWeapon = buffer_read(bufferServerReceived, buffer_u16);
         if (!is_undefined(ds_map_find_value(global.Clients, thisID))){
            var inst = ds_map_find_value(global.Clients, thisID);
            scrServerAttack(thisDirection, inst, thisWeapon);
         }
         break;
    // A player fired a bow, so we need to create an arrow and send it to everyone
    case MSG_BOW:
        var thisID = buffer_read(bufferServerReceived, buffer_u32);
        var thisDirection = buffer_read(bufferServerReceived, buffer_string);
        var thisAngle = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.Clients, thisID))){
            var inst = ds_map_find_value(global.Clients, thisID);
            scrServerBowAttack(thisDirection, inst, thisAngle);
        }
        break;
    // A player fired a gun, so we need to create one bullet and send it to everyone
    case MSG_GUN:
        var thisID = buffer_read(bufferServerReceived, buffer_u32);
        var thisDirection = buffer_read(bufferServerReceived, buffer_string);
        var thisAngle = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.Clients, thisID))){
            var inst = ds_map_find_value(global.Clients, thisID);
            scrServerGunAttack(thisDirection, inst, thisAngle);
        }
        break;
    // A player fired a shotgun, so we need to create the bullets and send it to everyone
    case MSG_SHOTGUN:
        var thisID = buffer_read(bufferServerReceived, buffer_u32);
        var thisDirection = buffer_read(bufferServerReceived, buffer_string);
        var thisAngle = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.Clients, thisID))){
            var inst = ds_map_find_value(global.Clients, thisID);
            scrServerShotgunAttack(thisDirection, inst, thisAngle);
        }
        break;
    // A player just died, so create a backpack and put it on the ground
    case MSG_BACKPACK:
        var itemCount = buffer_read(bufferServerReceived, buffer_u16);
        var itemsToAdd = ds_list_create();
        var quantitysToAdd = ds_list_create();
        for (var i = 0; i < itemCount; i++) {
            itemsToAdd[| i] = buffer_read(bufferServerReceived, buffer_u16);
            quantitysToAdd[| i] = buffer_read(bufferServerReceived, buffer_u16);
        }
        var playerX = buffer_read(bufferServerReceived, buffer_u16);
        var playerY = buffer_read(bufferServerReceived, buffer_u16);
        var backpackInstance = instance_create(playerX, playerY, objBackpackServer);
        ds_map_add(global.backpackMapServer, backpackInstance, backpackInstance);
        ds_list_copy(backpackInstance.items, itemsToAdd);
        ds_list_copy(backpackInstance.quantity, quantitysToAdd);
        //backpackInstance.items = itemsToAdd;
        //backpackInstance.quantity = quantitysToAdd;
        backpackInstance.alarm[0] = 1;
        break;
    // A player took an item from the backpack, so we need to update the backpack and send it to all the players!
    case MSG_TOOK_FROM_BACKPACK:
        var backpackID = buffer_read(bufferServerReceived, buffer_u32);
        show_debug_message("BACKPACK ID RECEIVED = " + string(backpackID));
        var itemPosition = buffer_read(bufferServerReceived, buffer_u16);
        if (!is_undefined(ds_map_find_value(global.backpackMapServer, backpackID))) {
            var inst = ds_map_find_value(global.backpackMapServer, backpackID);
            with (inst) {
                ds_list_delete(items, itemPosition);
                ds_list_delete(quantity, itemPosition);
                // This alarm will send the updated backpack to all the players!
                alarm[0] = 1;
            }
        }
        break;
    // A sound was played, send it to all the players
    case MSG_SOUND:
        var soundX = buffer_read(bufferServerReceived, buffer_u16);
        var soundY = buffer_read(bufferServerReceived, buffer_u16);
        var soundType = buffer_read(bufferServerReceived, buffer_u16);
        ServerSendSound(soundX, soundY, soundType);
}       
