///scrMovement(characterSpeed)
charSpeed = argument[0];
image_speed = 0.5;

if ( keyboard_check(vk_up) and keyboard_check(vk_left) )
{
    global.directionPlayer = "UP";
    
    if (image_index > playerMovement.upEnd or image_index < playerMovement.upStart) {
        image_index = playerMovement.upStart;
    }
    
    if (y > charSpeed and place_free(x, y -charSpeed))
    {
        y -= charSpeed;
    }
    if (x > charSpeed and place_free(x - charSpeed, y))
    {   
        x -= charSpeed;
    }
}
else if ( keyboard_check(vk_up) and keyboard_check(vk_right) )
{
    global.directionPlayer = "UP";
    
    if (image_index > playerMovement.upEnd or image_index < playerMovement.upStart) {
        image_index = playerMovement.upStart;
    }
    
    if (y > charSpeed and place_free(x, y -charSpeed))
    {
        y -= charSpeed;
    }
    if (x < room_width - charSpeed and place_free(x + charSpeed, y))
    {
        x += charSpeed;
    }
}
else if ( keyboard_check(vk_down) and keyboard_check(vk_left) )
{
    global.directionPlayer = "DOWN";
    
    if (image_index > playerMovement.downEnd or image_index < playerMovement.downStart) {
        image_index = playerMovement.downStart;
    }
    
    if (y < room_height - charSpeed and place_free(x, y +charSpeed))
    {
        y += charSpeed;
    }
    if (x > charSpeed and place_free(x - charSpeed, y))
    {   
        x -= charSpeed;
    }
}
else if ( keyboard_check(vk_down) and keyboard_check(vk_right) )
{
    global.directionPlayer = "DOWN";
    if (image_index > playerMovement.downEnd or image_index < playerMovement.downStart) {
        image_index = playerMovement.downStart;
    }
    if (y < room_height - charSpeed and place_free(x, y +charSpeed))
    {
        y += charSpeed;
    }
    if (x < room_width - charSpeed and place_free(x + charSpeed, y))
    {
        x += charSpeed;
    }
}
else if (keyboard_check(vk_left)) 
{
    global.directionPlayer = "LEFT";
    if (image_index > playerMovement.leftEnd or image_index < playerMovement.leftStart) {
        image_index = playerMovement.leftStart;
    }
    if (x > charSpeed and place_free(x - charSpeed, y))
    {   
        x -= charSpeed;
    }
}
else if (keyboard_check(vk_right))
{
    global.directionPlayer = "RIGHT";
    if (image_index > playerMovement.rightEnd or image_index < playerMovement.rightStart) {
        image_index = playerMovement.rightStart;
    }
    if (x < room_width - charSpeed and place_free(x + charSpeed, y))
    {
        x += charSpeed;
    }
}
else if (keyboard_check(vk_up)) 
{
    global.directionPlayer = "UP";
    if (image_index > playerMovement.upEnd or image_index < playerMovement.upStart) {
        image_index = playerMovement.upStart;
    }
    if (y > charSpeed and place_free(x, y -charSpeed))
    {
        y -= charSpeed;
    }
}
else if (keyboard_check(vk_down))
{
    global.directionPlayer = "DOWN";
    if (image_index > playerMovement.downEnd or image_index < playerMovement.downStart) {
        image_index = playerMovement.downStart;
    }
    if (y < room_height - charSpeed and place_free(x, y + charSpeed))
    {
        y += charSpeed;
    }
}
else 
{
    switch(global.directionPlayer) {
        case "UP":
            image_index = playerMovement.upStart;
            break;
        case "DOWN":
            image_index = playerMovement.downStart;
            break;
        case "LEFT":
            image_index = playerMovement.leftStart;
            break;
        case "RIGHT":
            image_index = playerMovement.rightStart;
            break;         
    }   
        image_speed = 0;
}
