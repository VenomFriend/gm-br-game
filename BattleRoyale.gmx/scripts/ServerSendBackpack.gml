///ServerSendBackpack(items, quantity, x, y, id)
//Send the backpack that was dropped to all the players

var items = argument[0];
var quantity = argument[1];
var thisX = argument[2];
var thisY = argument[3];
var thisID = argument[4];
var itemsCount = ds_list_size(items);
var socketSize = ds_list_size(global.socketList);

buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
buffer_write(global.bufferServerWrite, buffer_u8, MSG_BACKPACK);
buffer_write(global.bufferServerWrite, buffer_u16, itemsCount);
for (var i = 0; i < itemsCount; i++) {
    buffer_write(global.bufferServerWrite, buffer_u16, items[| i]);
    buffer_write(global.bufferServerWrite, buffer_u16, quantity[| i]);
}
buffer_write(global.bufferServerWrite, buffer_u16, thisX);
buffer_write(global.bufferServerWrite, buffer_u16, thisY);
buffer_write(global.bufferServerWrite, buffer_u32, thisID);
for(var i = 0; i<socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
}
