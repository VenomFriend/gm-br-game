var playerAttacked = argument[0];
var playerWeapon = argument[1];
var socketSize = ds_list_size(global.socketList);
var playerAttackedID = -1;


mapFind = ds_map_find_first(global.Clients);
mapSize = ds_map_size(global.Clients);

for (var i = 0; i< mapSize; i++) {
    if (ds_map_find_value(global.Clients, mapFind) == playerAttacked) {
        playerAttackedID = mapFind;
        break;
    }
    mapFind = ds_map_find_next(global.Clients, mapFind);
}


if (playerAttackedID != -1) {
    buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
    buffer_write(global.bufferServerWrite, buffer_u8, MSG_DAMAGE);
    buffer_write(global.bufferServerWrite, buffer_u16, playerWeapon);
    for (var i = 0; i < socketSize; i++) {
        var thisSocket = ds_list_find_value(global.socketList, i);
        if (playerAttackedID == thisSocket) {
            network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
            break;
        }
    }
}

