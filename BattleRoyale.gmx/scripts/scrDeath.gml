///scrDeath()
global.playerDead = true;
image_index = playerDeath.start;
image_speed = 0.5;

//Find all of the player items and send them to the server
//so they appear inside a backpack on the ground!

var itemsToDrop = ds_list_create();
var quantityToDrop = ds_list_create();


if (global.weaponEquiped != inventory.none) {
    ds_list_add(itemsToDrop, global.weaponEquiped);
    ds_list_add(quantityToDrop, 1);
    //ClientSendDrop(global.weaponEquiped, 1, x, y);
}
if (global.ammoEquiped != inventory.none) {
    ds_list_add(itemsToDrop, global.ammoEquiped);
    ds_list_add(quantityToDrop, global.ammoCount);
    //ClientSendDrop(global.ammoEquiped, global.ammoCount, x, y);
}

for (var yy = 0; yy < playerInventory.inventoryHeight; yy ++ ) {
    for (var xx = 0; xx < playerInventory.inventoryWidth; xx ++) {
        var itemToSend = playerInventory.inventoryItems[# xx, yy];
        var itemQuantity = playerInventory.inventoryValues[# xx, yy];
        if (itemToSend != inventory.none) {
            ds_list_add(itemsToDrop, itemToSend);
            ds_list_add(quantityToDrop, itemQuantity);
            //ClientSendDrop(itemToSend, itemQuantity, x, y);
        }
    }
}

ClientSendDrop(itemsToDrop, quantityToDrop, x, y);

