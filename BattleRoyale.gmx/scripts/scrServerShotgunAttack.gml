///scrServerShotgunAttack(direction, player, angle)

var playerDirection = argument[0];
var playerInstance = argument[1];
var shotAngle = argument[2];

switch(playerDirection) {
    case "UP":
        with (playerInstance) {
        
            var bullet1 = instance_create(x, y - 32, objBullet);
            bullet1.sprite_index = sprBulletUp;
            bullet1.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet1);
            
            var bullet2 = instance_create(x, y - 32, objBullet);
            bullet2.sprite_index = sprBulletUp;
            bullet2.angle = shotAngle - 15;
            ds_list_add(global.bulletsListServer, bullet2);
            
            var bullet3 = instance_create(x, y - 32, objBullet);
            bullet3.sprite_index = sprBulletUp;
            bullet3.angle = shotAngle + 15;
            ds_list_add(global.bulletsListServer, bullet3);
            
        }
        break;
    case "DOWN":
        with (playerInstance) {
        
            var bullet1 = instance_create(x, y + 33, objBullet);
            bullet1.sprite_index = sprBulletDown;
            bullet1.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet1);
            
            var bullet2 = instance_create(x, y + 33, objBullet);
            bullet2.sprite_index = sprBulletDown;
            bullet2.angle = shotAngle - 15;
            ds_list_add(global.bulletsListServer, bullet2);
            
            var bullet3 = instance_create(x, y + 33, objBullet);
            bullet3.sprite_index = sprBulletDown;
            bullet3.angle = shotAngle - 15;
            ds_list_add(global.bulletsListServer, bullet3);
        }

        break;
    case "LEFT":
        with (playerInstance) {
            var bullet1 = instance_create(x - 47, y - 4, objBullet);
            bullet1.sprite_index = sprBulletLeft;
            bullet1.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet1);
            
            var bullet2 = instance_create(x - 47, y - 4, objBullet);
            bullet2.sprite_index = sprBulletLeft;
            bullet2.angle = shotAngle - 15;
            ds_list_add(global.bulletsListServer, bullet2);
            
            var bullet3 = instance_create(x - 47, y - 4, objBullet);
            bullet3.sprite_index = sprBulletLeft;
            bullet3.angle = shotAngle + 15;
            ds_list_add(global.bulletsListServer, bullet3);
        }

        break;
    case "RIGHT":
        with (playerInstance) {
            var bullet1 = instance_create(x + 47, y - 4, objBullet);
            bullet1.sprite_index = sprBulletRight;
            bullet1.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet1);
            
            var bullet2 = instance_create(x + 47, y - 4, objBullet);
            bullet2.sprite_index = sprBulletRight;
            bullet2.angle = shotAngle - 15;
            ds_list_add(global.bulletsListServer, bullet2);
            
            var bullet3 = instance_create(x + 47, y - 4, objBullet);
            bullet3.sprite_index = sprBulletRight;
            bullet3.angle = shotAngle + 15;
            ds_list_add(global.bulletsListServer, bullet3);
        }

        break;
}

ServerSendGunAttack(bullet1, bullet1.x, bullet1.y, bullet1.sprite_index);
ServerSendGunAttack(bullet2, bullet2.x, bullet2.y, bullet2.sprite_index);
ServerSendGunAttack(bullet3, bullet3.x, bullet3.y, bullet3.sprite_index);
