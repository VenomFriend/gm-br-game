/// scrClickInventoryY(mouseY)
// Returns the position Y of the inventory that you clicked
// or -1 if it was outside the inventory
var mouseY = argument[0];

if (mouseY < view_yview[0] + 59 or mouseY > view_yview[0] + 59 + 128) {
    return -1;
}


var yy = (mouseY - (view_yview[0] + 59)) div inventorySize;

return yy;
