/// scrClickInventoryY(mouseY)
// Returns the position Y of the inventory that you clicked
// or -1 if it was outside the inventory
var mouseY = argument[0];

if (mouseY < view_yview[0] + 150 or mouseY > view_yview[0] + 150 + 32) {
    return -1;
}


var yy = (mouseY - (view_yview[0] + 150)) div 32;

return yy;
