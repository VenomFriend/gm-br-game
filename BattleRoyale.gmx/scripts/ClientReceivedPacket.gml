var bufferClientReceived = argument[0]
var msgid = buffer_read(bufferClientReceived, buffer_u8);

switch(msgid) {
    //Set my ID to the one the server sent me
    //And send all my information to the server!
    case MSG_CREATE_ID:
        global.myPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        var inst = instance_find(objPlayer, 0);
        ClientSendPlayer(inst);
        break;
    
    //Create all the players!
    case MSG_CREATE_PLAYER:
        var playerCount = buffer_read(bufferClientReceived, buffer_u16);
        for (var i = 0; i < playerCount; i++) {
            var playerID = buffer_read(bufferClientReceived, buffer_u32);
            var playerX = buffer_read(bufferClientReceived, buffer_u16);
            var playerY = buffer_read(bufferClientReceived, buffer_u16);
            var playerSprite = buffer_read(bufferClientReceived, buffer_u16);
            var playerImage = buffer_read(bufferClientReceived, buffer_u16);
            var playerHair = buffer_read(bufferClientReceived, buffer_s16);
            var playerShirt = buffer_read(bufferClientReceived, buffer_s16);
            var playerLegs = buffer_read(bufferClientReceived, buffer_s16);
            var playerHat = buffer_read(bufferClientReceived, buffer_s16);
            var playerShoes = buffer_read(bufferClientReceived, buffer_s16);
            var playerHairColor = buffer_read(bufferClientReceived, buffer_s32);
            var playerShirtColor = buffer_read(bufferClientReceived, buffer_s32);
            var playerLegsColor = buffer_read(bufferClientReceived, buffer_s32);
            var playerHatColor = buffer_read(bufferClientReceived, buffer_s32);
            var playerShoesColor = buffer_read(bufferClientReceived, buffer_s32);
            var playerHP = buffer_read(bufferClientReceived, buffer_u16);
            
            //If the player wasn't already created and it's not myself, create him!
            if ( (playerID == global.myPlayerID) ) {
                break;
            } else if (is_undefined(ds_map_find_value(Players, playerID))) {
                var newPlayer = instance_create(playerX, playerY, objDummieClient);
                with (newPlayer) {
                    sprite_index = playerSprite;
                    image_index = playerImage;
                    myHair = playerHair;
                    myShirt = playerShirt;
                    myLegs = playerLegs;
                    myHat = playerHat;
                    myShoes = playerShoes;
                    myHairColor = playerHairColor;
                    myShirtColor = playerShirtColor;
                    myLegsColor = playerLegsColor;
                    myHatColor = playerHatColor;
                    myShoesColor = playerShoesColor;
                    playerLife = playerHP;
                }
                ds_map_add(Players, playerID, newPlayer);
            } else {
                var thisPlayer = ds_map_find_value(Players, playerID);
                with (thisPlayer) {
                    sprite_index = playerSprite;
                    image_index = playerImage;
                    myHair = playerHair;
                    myShirt = playerShirt;
                    myLegs = playerLegs;
                    myHat = playerHat;
                    myShoes = playerShoes;
                    myHairColor = playerHairColor;
                    myShirtColor = playerShirtColor;
                    myLegsColor = playerLegsColor;
                    myHatColor = playerHatColor;
                    myShoesColor = playerShoesColor;
                    playerLife = playerHP;
                }
            }        
        }
        break;
        
    //A player disconnected, so delete him!
    case MSG_DELETE_PLAYER:
        thisPlayerID = buffer_read(bufferClientReceived, buffer_u32);
        if (!is_undefined(ds_map_find_value(Players, thisPlayerID))){
            var inst = ds_map_find_value(Players, thisPlayerID);
            ds_map_delete(Players, thisPlayerID);
            with (inst) {
                instance_destroy();
            }
        }
        break;
        
    //Someone moved/changed sprite/whatever, update it!    
    case MSG_MOVEMENT:
        var thisID = buffer_read(bufferClientReceived, buffer_u32);
        var thisX = buffer_read(bufferClientReceived, buffer_u16);
        var thisY = buffer_read(bufferClientReceived, buffer_u16);
        var thisImage = buffer_read(bufferClientReceived, buffer_u16);
        var thisMeleeWeapon = buffer_read(bufferClientReceived, buffer_u16);
        var thisLife = buffer_read(bufferClientReceived, buffer_u16);
        if (is_undefined(ds_map_find_value(Players, thisID)) and thisID != global.myPlayerID )
        {         
            var inst = instance_create(thisX, thisY, objDummieClient);
            ds_map_add(Players, thisID, inst);
        }
        else {
            var inst = ds_map_find_value(Players, thisID);
        }
        //Probably gonna need to find a better way to do this
        //but basically, with this I won't be updating my own player twice
        //(once when a click, and once when I receive it from the server)
        if (thisID != global.myPlayerID) {
            inst.x = thisX;
            inst.y = thisY;
            inst.image_index = thisImage;   
            inst.myMeleeWeapon = thisMeleeWeapon;
            inst.playerLife = thisLife;
        }
        break;
        
    //I took damage!
    case MSG_DAMAGE:
        var thisWeapon = buffer_read(bufferClientReceived, buffer_u16);
        //Fist
        if thisWeapon == inventory.none {
            scrLoseLife(2);
        }
        //Knife
        else if thisWeapon == inventory.knife {
            scrLoseLife(4);
        }
        //Arrow
        else if thisWeapon == inventory.bow {
            scrLoseLife(5);
        }
        //Bullet
        else if thisWeapon == inventory.gun {
            scrLoseLife(6);
        }
        //Bullet
        else if thisWeapon == inventory.rapier {
            scrLoseLife(5);
        }
        if global.playerLife < 0 {
            global.playerLife = 0;
        }
        // Send the message to the server after losing life
        break;
    
    //An arrow was created!
    case MSG_BOW:
        var arrowID = buffer_read(bufferClientReceived, buffer_u32);
        var arrowX = buffer_read(bufferClientReceived, buffer_u16);
        var arrowY = buffer_read(bufferClientReceived, buffer_u16);
        var arrowSprite = buffer_read(bufferClientReceived, buffer_u16); 
        var arrow = instance_create(arrowX, arrowY, objArrowDummie);
        arrow.sprite_index = arrowSprite;
        ds_map_add(global.arrowsMapClient, arrowID, arrow);
        break;
    
    //An arrow was destroyed!
    case MSG_DESTROY_ARROW:
        var arrowID = buffer_read(bufferClientReceived, buffer_u32);
        var arrow = ds_map_find_value(global.arrowsMapClient, arrowID);
        with (arrow){
            instance_destroy();
        }
        break;
    
    //Update the arrow position!
    case MSG_ARROW_POSITION:
        var arrowID = buffer_read(bufferClientReceived, buffer_u32);
        var thisX = buffer_read(bufferClientReceived, buffer_u16);
        var thisY = buffer_read(bufferClientReceived, buffer_u16);
        var arrow = ds_map_find_value(global.arrowsMapClient, arrowID);
        if (is_undefined(ds_map_find_value(global.arrowsMapClient, arrowID)) )
        {         
            var arrow = instance_create(thisX, thisY, objArrowDummie);
            ds_map_add(global.arrowsMapClient, arrowID, arrow);
        }
        with (arrow){
            x = thisX;
            y = thisY;
        }
        break;
        
    //A bullet was created!
    case MSG_GUN:
        var bulletID = buffer_read(bufferClientReceived, buffer_u32);
        var bulletX = buffer_read(bufferClientReceived, buffer_u16);
        var bulletY = buffer_read(bufferClientReceived, buffer_u16);
        var bulletSprite = buffer_read(bufferClientReceived, buffer_u16); 
        var bullet = instance_create(bulletX, bulletY, objBulletDummie);
        bullet.sprite_index = bulletSprite;
        ds_map_add(global.bulletsMapClient, bulletID, bullet);
        break;
        
    //A bullet was destroyed!
    case MSG_DESTROY_BULLET:
        var bulletID = buffer_read(bufferClientReceived, buffer_u32);
        var bullet = ds_map_find_value(global.bulletsMapClient, bulletID);
        with (bullet){
            instance_destroy();
        }
        break;
        
    //Update the bullet position!
    case MSG_BULLET_POSITION:
        var bulletID = buffer_read(bufferClientReceived, buffer_u32);
        var thisX = buffer_read(bufferClientReceived, buffer_u16);
        var thisY = buffer_read(bufferClientReceived, buffer_u16);
        var bullet = ds_map_find_value(global.bulletsMapClient, bulletID);
        if (is_undefined(ds_map_find_value(global.bulletsMapClient, bulletID)) )
        {         
            var bullet = instance_create(thisX, thisY, objBulletDummie);
            ds_map_add(global.bulletsMapClient, bulletID, bullet);
        }
        with (bullet){
            x = thisX;
            y = thisY;
        }
        break;
        
    //A backpack was dropped!
    case MSG_BACKPACK:
        var itemCount = buffer_read(bufferClientReceived, buffer_u16);
        var itemsToAdd = ds_list_create();
        var quantitysToAdd = ds_list_create();
        for (var i = 0; i < itemCount; i++) {
            itemsToAdd[| i] = buffer_read(bufferClientReceived, buffer_u16);
            quantitysToAdd[| i] = buffer_read(bufferClientReceived, buffer_u16);
        }
        var playerX = buffer_read(bufferClientReceived, buffer_u16);
        var playerY = buffer_read(bufferClientReceived, buffer_u16);
        var backpackID = buffer_read(bufferClientReceived, buffer_u32);
        var backpackInstance = ds_map_find_value(global.backpacksMapClient, backpackID);
        if (is_undefined(ds_map_find_value(global.backpacksMapClient, backpackID)) )
        {         
            var backpackInstance = instance_create(playerX, playerY, objBackpackDummie);
            ds_map_add(global.backpacksMapClient, backpackID, backpackInstance);
        }
        ds_list_copy(backpackInstance.items, itemsToAdd);
        ds_list_copy(backpackInstance.quantity, quantitysToAdd);
        with (backpackInstance) {
            myID = backpackID;
        }
        //backpackInstance.items = itemsToAdd;
        //backpackInstance.quantity = quantitysToAdd;
        break;
        
    //A backpack is empty, so delete it!
    case MSG_DELETE_BACKPACK:
        thisBackpackID = buffer_read(bufferClientReceived, buffer_u32);
        if (!is_undefined(ds_map_find_value(global.backpacksMapClient, thisBackpackID))){
            var inst = ds_map_find_value(global.backpacksMapClient, thisBackpackID);
            ds_map_delete(global.backpacksMapClient, thisBackpackID);
            with (inst) {
                instance_destroy();
            }
        }
        break;
    case MSG_SOUND:
        var soundX = buffer_read(bufferClientReceived, buffer_u16);
        var soundY = buffer_read(bufferClientReceived, buffer_u16);
        var soundType = buffer_read(bufferClientReceived, buffer_u16);
        switch(soundType) {
            case soundEffect.bowShot:
                break;
            case soundEffect.gunShot:
                instance_create(soundX, soundY, objGunShot);
                break;
            case soundEffect.gunReload:
                instance_create(soundX, soundY, objGunReload);
                break;
            case soundEffect.shotgunShot:
                instance_create(soundX, soundY, objShotgunShot);
                break;
            case soundEffect.shotgunReloadAll:
                instance_create(soundX, soundY, objShotgunReloadAll);
                break;
            case soundEffect.shotgunEmpty:
                instance_create(soundX, soundY, objShotgunEmpty);
                break;
        }
}
