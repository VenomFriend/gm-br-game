var socketSize = ds_list_size(global.socketList);

for (var i =0; i<socketSize; i++) {
    var thisPlayerID = ds_list_find_value(global.socketList, i);
    var inst = ds_map_find_value(global.Clients, thisPlayerID);
    
    //Only send the info of the players who actually changed something, to avoid wasting resources!
    if (inst.x != inst.xprevious) or (inst.y != inst.yprevious) or (inst.image_index != inst.currentImage) or (inst.playerLife != inst.currentLife) {
        
        inst.currentImage = inst.image_index;
        inst.currentLife = inst.playerLife;    
    
        buffer_seek(global.bufferServerWrite, buffer_seek_start, 0);
        buffer_write(global.bufferServerWrite, buffer_u8, MSG_MOVEMENT);
        buffer_write(global.bufferServerWrite, buffer_u32, thisPlayerID);
        buffer_write(global.bufferServerWrite, buffer_u16, inst.x);
        buffer_write(global.bufferServerWrite, buffer_u16, inst.y);
        buffer_write(global.bufferServerWrite, buffer_u16, inst.image_index);
        buffer_write(global.bufferServerWrite, buffer_u16, inst.myMeleeWeapon);
        buffer_write(global.bufferServerWrite, buffer_u16, inst.playerLife);
        
        for(var ii = 0; ii<socketSize; ii++) {
            var thisSocket = ds_list_find_value(global.socketList, ii);
            if (thisPlayerID != thisSocket) {
                network_send_packet(thisSocket, global.bufferServerWrite, buffer_tell(global.bufferServerWrite));
            }
        }
    }
}
