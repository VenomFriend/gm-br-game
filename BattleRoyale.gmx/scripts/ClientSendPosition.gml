var inst = argument[0];

buffer_seek(global.bufferClientWrite, buffer_seek_start, 0);
buffer_write(global.bufferClientWrite, buffer_u8, MSG_MOVEMENT);
buffer_write(global.bufferClientWrite, buffer_u32, global.myPlayerID);
buffer_write(global.bufferClientWrite, buffer_u16, inst.x);
buffer_write(global.bufferClientWrite, buffer_u16, inst.y);
buffer_write(global.bufferClientWrite, buffer_u16, inst.image_index);
buffer_write(global.bufferClientWrite, buffer_u16, global.playerLife);
network_send_packet(global.socket, global.bufferClientWrite, buffer_tell(global.bufferClientWrite));
