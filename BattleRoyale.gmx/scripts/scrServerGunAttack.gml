///scrServerGunAttack(direction, player, angle)

var playerDirection = argument[0];
var playerInstance = argument[1];
var shotAngle = argument[2];

switch(playerDirection) {
    case "UP":
        with (playerInstance) {
            var bullet = instance_create(x, y - 32, objBullet);
            bullet.sprite_index = sprBulletUp;
            bullet.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet);
        }
        break;
    case "DOWN":
        with (playerInstance) {
            var bullet = instance_create(x, y + 33, objBullet);
            bullet.sprite_index = sprBulletDown;
            bullet.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet);
        }

        break;
    case "LEFT":
        with (playerInstance) {
            var bullet = instance_create(x - 47, y - 4, objBullet);
            bullet.sprite_index = sprBulletLeft;
            bullet.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet);
        }

        break;
    case "RIGHT":
        with (playerInstance) {
            var bullet = instance_create(x + 47, y - 4, objBullet);
            bullet.sprite_index = sprBulletRight;
            bullet.angle = shotAngle;
            ds_list_add(global.bulletsListServer, bullet);
        }

        break;
}

ServerSendGunAttack(bullet, bullet.x, bullet.y, bullet.sprite_index);
