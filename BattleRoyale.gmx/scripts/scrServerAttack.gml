///scrServerAttack(direction, player, weapon)

var playerDirection = argument[0];
var playerInstance = argument[1];
var playerWeapon = argument[2];

if (playerWeapon == inventory.none or playerWeapon == inventory.knife) {
    switch(playerDirection) {
        case "UP":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x - 16 , y, x + 16, y - 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
        case "DOWN":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x - 16 , y, x + 16, y + 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
        case "LEFT":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x, y - 16, x - 32, y + 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
        case "RIGHT":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x , y -16, x + 32, y + 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
    }
} else if (playerWeapon == inventory.rapier) {
    switch(playerDirection) {
        case "UP":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x - 16 , y, x + 16, y - 36, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
        case "DOWN":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x - 16 , y, x + 16, y + 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
        case "LEFT":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x, y - 16, x - 80, y + 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
        case "RIGHT":
            with (playerInstance) {
                myMeleeWeapon = playerWeapon;
                var enemy = collision_rectangle( x , y -16, x + 80, y + 48, objDummieServer, false, true );
            }
            if enemy != noone {
                ServerSendAttack(enemy, playerWeapon);
            }
            break;
    }
}
